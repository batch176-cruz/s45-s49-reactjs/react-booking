import Banner from './../components/Banner';
import Highlights from './../components/Highlights';



export default function Home() {
	return(

		<>
			<Banner name="Janne" age={25}/>
			<Highlights/>
			
		</>

		)
}