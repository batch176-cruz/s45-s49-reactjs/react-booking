const coursesData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem, ipsum dolor sit amet consectetur adipisicing, elit. Illo similique fugiat tenetur laudantium, nobis commodi impedit consectetur minima distinctio. Molestias accusamus perferendis eum magni incidunt vitae distinctio ducimus repellendus sapiente.",
		price: 45000
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem, ipsum dolor sit amet consectetur adipisicing, elit. Illo similique fugiat tenetur laudantium, nobis commodi impedit consectetur minima distinctio. Molestias accusamus perferendis eum magni incidunt vitae distinctio ducimus repellendus sapiente.",
		price: 50000
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem, ipsum dolor sit amet consectetur adipisicing, elit. Illo similique fugiat tenetur laudantium, nobis commodi impedit consectetur minima distinctio. Molestias accusamus perferendis eum magni incidunt vitae distinctio ducimus repellendus sapiente.",
		price: 55000
	}
]

export default coursesData;